/*
 * Copyright:Copyright(c)2017-2020
 * Company:ygego
 */
package com.example.springbootelasticjob.job;

import com.alibaba.fastjson.JSON;
import com.dangdang.ddframe.job.api.ShardingContext;
import com.dangdang.ddframe.job.api.simple.SimpleJob;
import com.example.springbootelasticjob.dto.ActivityIdDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * ClassName: TestJob
 * Description:
 * Author: zhaojinpeng
 * Date: 2018/6/8 14:52
 * History:
 * <zhaojinpeng> <2018/6/8 14:52> <1.0>    <desc>
 * 作者姓名 修改时间    版本号 描述
 */
@Component
public class TestJob implements SimpleJob {

    private final static Logger logger = LoggerFactory.getLogger(TestJob.class);

    @Override
    public void execute(ShardingContext shardingContext) {
        String jobParameter = shardingContext.getJobParameter();
        ActivityIdDTO activityIdDTO = JSON.parseObject(jobParameter, ActivityIdDTO.class);
        logger.info("任务名：{}, 片数：{}, id={}", shardingContext.getJobName(), shardingContext.getShardingTotalCount(),
                shardingContext.getJobParameter());
    }

}
