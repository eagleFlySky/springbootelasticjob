/*
 * Copyright: Copyright (c) 2017-2020
 * Company: ygego
 */

package com.example.springbootelasticjob.dto;

import javax.validation.constraints.NotNull;

/**
 * ClassName: ActivityIdDTO
 * Description: 专门用来接收前台传过来的活动ID参数
 * Author: sunyang
 * Date: 24/03/2018 3:02 PM
 * History:
 * <author>          <time>          <version>          <desc>
 * 作者姓名           修改时间           版本号              描述
 */
public class ActivityIdDTO {

    @NotNull(message = ":活动ID不能为空")
    private Long bidActivityId;

    public Long getBidActivityId() {
        return bidActivityId;
    }

    public void setBidActivityId(Long bidActivityId) {
        this.bidActivityId = bidActivityId;
    }
}
