/*
 * Copyright:Copyright(c)2017-2020
 * Company:ygego
 */
package com.example.springbootelasticjob.config;

import com.dangdang.ddframe.job.api.JobAPIFactory;
import com.dangdang.ddframe.job.api.JobOperateAPI;
import com.dangdang.ddframe.job.api.JobSettingsAPI;
import com.dangdang.ddframe.job.api.JobStatisticsAPI;
import com.google.common.base.Optional;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * ClassName: JobConfig
 * Description:
 * Author: zhaojinpeng
 * Date: 2018/6/8 17:29
 * History:
 * <zhaojinpeng> <2018/6/8 17:29> <1.0>    <desc>
 * 作者姓名 修改时间    版本号 描述
 */
@Configuration
public class JobConfig {

    @Value("${spring.elasticjob.zookeeper.server-lists}")
    private String connectString;
    @Value("${spring.elasticjob.zookeeper.namespace}")
    private String namespace;

    @Bean
    public MyElasticJobListener elasticJobListener() {
        return new MyElasticJobListener(100, 100);
    }

    @Bean
    public JobOperateAPI jobOperateAPI() {
        return JobAPIFactory.createJobOperateAPI(connectString, namespace, Optional.absent());
    }
    @Bean
    public JobSettingsAPI jobSettingsAPI() {
        return JobAPIFactory.createJobSettingsAPI(connectString, namespace, Optional.absent());
    }
    @Bean
    public JobStatisticsAPI jobStatisticsAPI() {
        return JobAPIFactory.createJobStatisticsAPI(connectString, namespace, Optional.absent());
    }

}
