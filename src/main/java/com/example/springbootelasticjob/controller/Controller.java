/*
 * Copyright:Copyright(c)2017-2020
 * Company:ygego
 */
package com.example.springbootelasticjob.controller;

import com.example.springbootelasticjob.config.ElasticJobHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * ClassName: Controller
 * Description:
 * Author: zhaojinpeng
 * Date: 2018/6/8 16:36
 * History:
 * <zhaojinpeng> <2018/6/8 16:36> <1.0>    <desc>
 * 作者姓名 修改时间    版本号 描述
 */
@RestController
@RequestMapping("demo")
public class Controller {

    @Autowired
    private ElasticJobHandler elasticJobHandler;

    @RequestMapping("addJob")
    public String addJob(@RequestParam Long id) {
        String cron = getCron(Date.from(LocalDateTime.now().plusMinutes(1).atZone(ZoneId.systemDefault()).toInstant()));
        //String cron = "*/10 * * * * ?";
        elasticJobHandler.addJob("测试", cron, 1, "{bidActivityId:12}");
        return "成功！";
    }

    private static final String CRON_DATE_FORMAT = "ss mm HH dd MM ? yyyy";

    /***
     *
     * @param date 时间
     * @return cron类型的日期
     */
    private String getCron(final Date date) {
        SimpleDateFormat sdf = new SimpleDateFormat(CRON_DATE_FORMAT);
        String formatTimeStr = "";
        if (date != null) {
            formatTimeStr = sdf.format(date);
        }
        return formatTimeStr;
    }
}
